
SHELL=bash # e.g. for >() for splitting output (not used): http://stackoverflow.com/questions/692000/how-do-i-write-stderr-to-a-file-while-using-tee-with-a-pipe#692407

RES=1200


.PHONY: all clean res

all: res

clean: 
	latexmk -C # *.aux *.log *.out *.pdf
	-rm -v *.bib jabref/tot.bib
	-rm -v *.bbl *.run.xml *.nav *.snm
	-rm figs/*.pdf figs/*.pdf_tex # from SVG conversion

res: res/SA-alv_poster.pdf

res/%.pdf : %.pdf
	sed "s|^/CreationDate (.*)$$|/CreationDate (D:20170130000000+00'00')|; \
	     s|^/ModDate (.*)$$|/ModDate (D:20170130000000+00'00')|; \
	     /^\/ID \[<.*> <.*>\]$$/d" $<  >  $@ # deleting ID instead of replacing: http://superuser.com/questions/130347/how-do-i-produce-bytewise-consistent-documents-with-pdflatex#130804

.PHONY: .refresh # to force running of latexmk
.refresh: # https://www.drewsilcock.co.uk/using-make-and-latexmk
	touch .refresh

figs/qrcode.svg:
	qrencode -m 0 -t SVG -o $@ http://osf.io/fy8ra/

%.pdf : %.tex %.bib .refresh figs/qrcode.svg
	latexmk -pdf -pdflatex="pdflatex --shell-escape %O %S" $< # see -pdflatex in manpage!

%s.pdf : %.pdf
	ps2pdf -dColorImageResolution=$(RES) -dGrayImageResolution=$(RES)  $< $@

jabref/tot.bib : jabref/
	$(MAKE) -C $<

%.bcf : %.tex
	pdflatex -shell-escape $<

%.bib : jabref/tot.bib %.bcf
	cp $< $@
	-biber -w --output-format=bibtex --output-resolve $* | grep --color 'WARN' # exit on grep warn: http://stackoverflow.com/questions/1545985/gnu-make-inverting-sub-process-success#1545996 # http://tex.stackexchange.com/questions/277289/biber-in-tool-mode-specify-data-source-when-creating-bib-file-containing-cited#277316
	mv $*_biber.bib $@ # biber cannot rw to same file

.SECONDARY: 
